﻿namespace NawadataTest

/* Algoritma:
* 1. Input the number of families (n) and the number of members per family.
* 2. Validate input:
*    - Check if the number of inputted members equals n.
*    - If not, print error "Input must be equal with count of family" and exit.
* 3. Calculate the total number of family members by summing the inputted numbers.
* 4. Calculate the total minimum number of buses:
*    - Divide the total number of family members by 4 (max bus capacity).
*    - If there is a remainder from the division (total family members % 4 != 0), add one additional bus.
* 5. Print the minimum number of buses required.
*/

{
    public class Soal2
    {
        public void Run()
        {
            Console.Write("Input the number of families: ");
            int n = int.Parse(Console.ReadLine());

            Console.Write("Input the number of members in the family (separated by a space): ");
            string input = Console.ReadLine();

            string[] membersInput = input.Split(' ');

            if (membersInput.Length != n)
            {
                Console.WriteLine("Input must be equal with count of family");
                return;
            }

            int[] members = new int[n];
            for (int i = 0; i < n; i++)
            {
                members[i] = int.Parse(membersInput[i]);
            }

            int totalMembers = 0;
            for (int i = 0; i < members.Length; i++)
            {
                totalMembers += members[i];
            }

            int busRequired = totalMembers / 4;
            if (totalMembers % 4 != 0)
            {
                busRequired++;
            }

            Console.WriteLine($"Minimum bus required is: {busRequired}");

        }
    }
}
