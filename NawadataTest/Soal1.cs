﻿namespace NawadataTest

/*
* 1. Input a line of words.
* 2. Initialize vowel and consonant counts to 0.
* 3. Iterate over each character in the input:
*    - Convert character to lowercase.
*    - If character is a letter (excluding spaces), check if it's a vowel or consonant.
*    - If it's a vowel, add to the vowel array and increment vowel count.
*    - If it's a consonant, add to the consonant array and increment consonant count.
* 4. Sort both vowel and consonant arrays.
* 5. Print the sorted arrays of vowels and consonants.
*/

{
    public class Soal1
    {
        public string procVowel(string param)
        {
            char[] vowels = new char[param.Length];
            int vowelCount = 0;
            foreach (var item in param.ToLower())
            {
                if (item == 'a' || item == 'e' || item == 'i' || item == 'o' || item == 'u')
                {
                    vowels[vowelCount++] = item;
                }
            }
            Array.Sort(vowels, 0, vowelCount);
            return new String(vowels, 0, vowelCount);
        }

        public string procConsonant(string param)
        {
            char[] consonants = new char[param.Length];
            int consonantCount = 0;
            foreach (var item in param.ToLower())
            {
                if (item is >= 'a' and <= 'z' and not ('a' or 'e' or 'i' or 'o' or 'u'))
                {
                    consonants[consonantCount++] = item;
                }
            }
            Array.Sort(consonants, 0, consonantCount);
            return new String(consonants, 0, consonantCount);
        }

        public void Run()
        {
            Console.Write("Input one line of words (S) : ");
            string input = Console.ReadLine();

            string charVowel = procVowel(input);
            string charConsonant = procConsonant(input);

            Console.WriteLine("Vowel Characters : ");
            Console.WriteLine(charVowel);
            Console.WriteLine("Consonant Characters : ");
            Console.WriteLine(charConsonant);
        }
    }
}
