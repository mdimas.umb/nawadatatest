﻿using Microsoft.VisualBasic;
using NawadataTest;
using System;


class Program
{
    static void Main(string[] args)
    {
        while (true)
        {
            Console.WriteLine("Pilih program yang akan dijalankan:");
            Console.WriteLine("1. Soal 1");
            Console.WriteLine("2. Soal 2");
            Console.WriteLine("3. Keluar");
            Console.Write("Masukkan pilihan (1/2/3): ");

            string pilihan = Console.ReadLine();

            if (pilihan == "1")
            {
                Soal1 soal1 = new Soal1();
                soal1.Run();
            }
            else if (pilihan == "2")
            {
                Soal2 soal2 = new Soal2();
                soal2.Run();
            }
            else if (pilihan == "3")
            {
                Console.WriteLine("Terima kasih! Program selesai.");
                break;
            }
            else
            {
                Console.WriteLine("Pilihan tidak valid!");
            }
        }

    }
}
